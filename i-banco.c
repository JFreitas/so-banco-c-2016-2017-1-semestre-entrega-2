/*
// Projeto SO - exercicio 1, version 1
// Sistemas Operativos, DEI/IST/ULisboa 2016-17
// Grupo 73 - Alameda
*/

#include "commandlinereader.h"
#include "contas.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#define COMANDO_DEBITAR "debitar"
#define COMANDO_CREDITAR "creditar"
#define COMANDO_LER_SALDO "lerSaldo"
#define COMANDO_SIMULAR "simular"
#define COMANDO_SAIR "sair"
#define COMANDO_AGORA "agora"

#define MAXARGS 3
#define BUFFER_SIZE 100

#define NUM_TRABALHADORAS 3
#define CMD_BUFFER_DIM 6
#define OP_LERSALDO 0
#define OP_CREDITAR 1
#define OP_DEBITAR 2
#define OP_NENHUM -1
#define OP_SAIR -2

typedef struct
{
    int operacao;
    int idConta;
    int valor;
} comando_t;


comando_t cmd_buffer[CMD_BUFFER_DIM]; // buffer circular
int buff_write_idx = 0, buff_read_idx = 0;


sem_t semBuffer, semRun;
pthread_mutex_t trincos[CMD_BUFFER_DIM], trinco_buff_read;

void insere_buffer(comando_t comando) {
    while(1) {
        sem_wait(&semBuffer); // espera se o buffer estiver cheio
        pthread_mutex_lock(&trincos[buff_write_idx]);
        if (cmd_buffer[buff_write_idx].operacao == OP_NENHUM) {
            cmd_buffer[buff_write_idx] = comando;
            pthread_mutex_unlock(&trincos[buff_write_idx]);
            buff_write_idx = (buff_write_idx + 1) % CMD_BUFFER_DIM;
            sem_post(&semRun);
            return;
        }
        pthread_mutex_unlock(&trincos[buff_write_idx]);
        buff_write_idx = (buff_write_idx + 1) % CMD_BUFFER_DIM;
    }
}

void *run_comando() {
    int saldo;
    while (1) {
    	pthread_mutex_lock(&trinco_buff_read);
    	int pos = buff_read_idx;
    	pthread_mutex_unlock(&trinco_buff_read);
        sem_wait(&semRun);//aguarda que sejam adicionados comandos no buffer
        pthread_mutex_lock(&trincos[pos]);
        if (cmd_buffer[pos].operacao != OP_NENHUM) {
            switch (cmd_buffer[pos].operacao) {
            case OP_LERSALDO:
                    saldo = lerSaldo(cmd_buffer[pos].idConta);
                    if (saldo < 0)
                        printf("%s(%d): Erro.\n\n", COMANDO_LER_SALDO, cmd_buffer[pos].idConta);
                    else
                        printf("%s(%d): O saldo da conta é %d.\n\n", COMANDO_LER_SALDO, cmd_buffer[pos].idConta, saldo);
                break;
            case OP_CREDITAR:
                    if (creditar (cmd_buffer[pos].idConta, cmd_buffer[pos].valor) < 0)
                        printf("%s(%d, %d): Erro\n\n", COMANDO_CREDITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
                    else
                        printf("%s(%d, %d): OK\n\n", COMANDO_CREDITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
                break;
            case OP_DEBITAR:
                    if (debitar (cmd_buffer[pos].idConta, cmd_buffer[pos].valor) < 0)
                        printf("%s(%d, %d): Erro\n\n", COMANDO_DEBITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
                    else
                        printf("%s(%d, %d): OK\n\n", COMANDO_DEBITAR, cmd_buffer[pos].idConta, cmd_buffer[pos].valor);
                break;
            case OP_SAIR:
            		pthread_mutex_unlock(&trincos[pos]);
                    pthread_exit(NULL);
                    break;
            default:
                break;
            }
            cmd_buffer[pos].operacao = OP_NENHUM;
            pthread_mutex_unlock(&trincos[pos]);
            sem_post(&semBuffer);
        }
        else {
            pthread_mutex_unlock(&trincos[pos]);
            sem_post(&semRun); 
        }

        pthread_mutex_lock(&trinco_buff_read);
        buff_read_idx = (buff_read_idx + 1) % CMD_BUFFER_DIM;
		pthread_mutex_unlock(&trinco_buff_read);        
        
    }
}

int main (int argc, char** argv) {
    
    pthread_t tid[NUM_TRABALHADORAS];

    char *args[MAXARGS + 1];
    char buffer[BUFFER_SIZE];
    
    signal(SIGUSR1, alteraFlag);
    inicializarContas();

    sem_init(&semBuffer,0,6);
    sem_init(&semRun,0,0);

    inicializarTrincos();

    pthread_mutex_init(&trinco_buff_read, NULL);
    
	for (int i = 0; i < CMD_BUFFER_DIM; i++) {
  		cmd_buffer[i].operacao = -1;
  		pthread_mutex_init(&trincos[i], NULL);
	}
    
    for (int i = 0; i < NUM_TRABALHADORAS; i++) {
        pthread_create(&tid[i],0,run_comando,NULL);
    }
    
    printf("Bem-vinda/o ao i-banco\n\n");
      
    while (1) {
        int numargs;
        comando_t comando;
        numargs = readLineArguments(args, MAXARGS+1, buffer, BUFFER_SIZE);
        /* EOF (end of file) do stdin ou comando "sair" */
        if (numargs < 0 ||
	        (numargs > 0 && (strcmp(args[0], COMANDO_SAIR) == 0))) {
            
            int pid2 = 0, status;
            
            if (numargs > 1 && (strcmp(args[1], COMANDO_AGORA) == 0)) {
            	/* Sair agora */
                kill(0,SIGUSR1); /* Envia sinal aos filhos para encerrarem apos terminarem 
				de imprimir o ano */
                while((pid2 = wait(&status)) > 0) {
                    // Espera que todos os filhos tenham terminado
                }
            }
            else {
                /* Sair */
                printf("i-banco vai terminar.\n--\n");
                
                while((pid2 = wait(&status)) > 0) {
                    if(WIFEXITED(status) > 0)
                        printf("FILHO TERMINADO (PID=%d; terminou normalmente)\n",pid2);
                    else if (WIFSIGNALED(status) > 0) {
                        printf("FILHO TERMINADO (PID=%d; terminou abruptamente)\n",pid2);
                    }
                }
                
                printf("--\ni-banco vai terminar.\n");
            }
        	while(1) {
                int boolean=1;
                for (int i=0; i < CMD_BUFFER_DIM; i++) {
                    pthread_mutex_lock(&trincos[i]);
                    boolean = boolean && (cmd_buffer[i].operacao == -1);
                    pthread_mutex_unlock(&trincos[i]);
                }
                if(boolean){//verifica se ja foram executados todos os comandos
                    comando.operacao = OP_SAIR;
                    comando.idConta = 0;
                    comando.valor = 0;
                    for (int i=0; i < NUM_TRABALHADORAS; i++) {//envia o comando para o thread terminar
                        insere_buffer(comando);
                    }
                    for (int i = 0; i < NUM_TRABALHADORAS; i++){//aguarda que as threads terminem
                        pthread_join(tid[i],NULL);
                    }
                    exit(EXIT_SUCCESS);
                }
                
            }
            
        }
    
        else if (numargs == 0)
            /* Nenhum argumento; ignora e volta a pedir */
            continue;
            
        /* Debitar */
        else if (strcmp(args[0], COMANDO_DEBITAR) == 0) {
            int idConta, valor;
            if (numargs < 3) {
                printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_DEBITAR);
	           continue;
            }

            idConta = atoi(args[1]);
            valor = atoi(args[2]);

            comando.operacao = OP_DEBITAR;
            comando.idConta = idConta;
            comando.valor = valor;
            insere_buffer(comando);
            
            //if (debitar (idConta, valor) < 0)
	        //   printf("%s(%d, %d): Erro\n\n", COMANDO_DEBITAR, idConta, valor);
            //else
            //    printf("%s(%d, %d): OK\n\n", COMANDO_DEBITAR, idConta, valor);
    }

    /* Creditar */
    else if (strcmp(args[0], COMANDO_CREDITAR) == 0) {
        int idConta, valor;
        if (numargs < 3) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_CREDITAR);
            continue;
        }

        idConta = atoi(args[1]);
        valor = atoi(args[2]);

        comando.operacao = OP_CREDITAR;
        comando.idConta = idConta;
        comando.valor = valor;
        insere_buffer(comando);
        
        
        /*if (creditar (idConta, valor) < 0)
            printf("%s(%d, %d): Erro\n\n", COMANDO_CREDITAR, idConta, valor);
        else
            printf("%s(%d, %d): OK\n\n", COMANDO_CREDITAR, idConta, valor);*/
    }

    /* Ler Saldo */
    else if (strcmp(args[0], COMANDO_LER_SALDO) == 0) {
        int idConta;

         if (numargs < 2) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_LER_SALDO);
            continue;
        }
        idConta = atoi(args[1]);
        //saldo = lerSaldo (idConta);
        
        comando.operacao = OP_LERSALDO;
        comando.idConta = idConta;
        comando.valor = 0;
        insere_buffer(comando);
        
        /*if (saldo < 0)
            printf("%s(%d): Erro.\n\n", COMANDO_LER_SALDO, idConta);
        else
            printf("%s(%d): O saldo da conta é %d.\n\n", COMANDO_LER_SALDO, idConta, saldo);*/
    }

    /* Simular */
    else if (strcmp(args[0], COMANDO_SIMULAR ) == 0) {
        int numAnos, adiciona;
        
        if (numargs < 3) {
            printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_SIMULAR);
            continue;
        }
        numAnos = atoi(args[1]);
        adiciona = atoi(args[2]);
        if (numAnos < 0) //numAnos não pode ser negativo
            printf("%s(%d): Erro.\n\n", COMANDO_SIMULAR, numAnos);
        else if (adiciona <= 0 || adiciona > 10)
            printf("%s(%d): Erro.\n\n", COMANDO_SIMULAR, adiciona); // numContas a simular não pode ser zero nem negativo nem maior que o número de contas possíveis
        else{
            int pid;
            pid = fork();
            if (pid == -1){
                printf("Failed to fork process 1\n");
                exit(1);
            }
            if (pid == 0){
                simular(numAnos,adiciona);
                exit(3);
            }
            else{
                continue;
            }
        }
    }
    else {
      printf("Comando desconhecido. Tente de novo.\n");
    }
  } 
}

