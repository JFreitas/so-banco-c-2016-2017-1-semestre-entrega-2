#include "contas.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define atrasar() sleep(ATRASO)

pthread_mutex_t trincos_contas[NUM_CONTAS];
		     
int contasSaldos[NUM_CONTAS];
int flag = 0;

void inicializarTrincos(){
  for (int i = 0; i < NUM_CONTAS; i++) {
    pthread_mutex_init(&trincos_contas[i], NULL);
  }
}

int contaExiste(int idConta) {
  return (idConta > 0 && idConta <= NUM_CONTAS);
}

void inicializarContas() {
  int i;
  for (i=0; i<NUM_CONTAS; i++)
    contasSaldos[i] = 0;
}

int debitar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  if (contasSaldos[idConta - 1] < valor){
    pthread_mutex_unlock(&trincos_contas[idConta-1]);
    return -1;
  }
  atrasar();
  contasSaldos[idConta - 1] -= valor;
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return 0;
}

int creditar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  contasSaldos[idConta - 1] += valor;
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return 0;
}

int lerSaldo(int idConta) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  pthread_mutex_lock(&trincos_contas[idConta-1]);
  int saldo = contasSaldos[idConta - 1];
  pthread_mutex_unlock(&trincos_contas[idConta-1]);
  return saldo;
}


void simular(int numAnos, int numContas) {
    int ano, i, max;
    
    // Imprime as estimativas até ao numAnos
    if (numContas > 0 && numContas <= 10){
    for (ano=0; ano <= numAnos; ano++) {
        printf("\nSIMULACAO: Ano %d\n",ano);
        printf("=================\n");
        for (i=0; i < numContas ; i++) { 
            printf("Conta %d, Saldo %d\n", i+1, lerSaldo(i+1));
            max = intMax(lerSaldo(i+1) * (1 + TAXAJURO) - CUSTOMANUTENCAO,0);
            if (max != 0) {
                creditar(i+1,lerSaldo(i+1) * TAXAJURO);
                debitar(i+1, CUSTOMANUTENCAO);
            }
        }
    }
        if (flag == 1) { // Verifica se a flag foi activada atraves do sinal SIGUSR1
            printf("Simulacao terminada por signal\n");
            exit(3);
        }
    }
    else{
        printf("Erro: numero de contas invalido\n");
    }
}

/*
 intMax:
 Recebe dois inteiros e devolve o maior
 */
int intMax(int a,int b) {
    if (a >= b)
        return a;
    else
        return b;
}
/*
 alteraFlag:
 funcao chamada quando e' recebido o sinal SIGUR1 e ativa a flag
*/
void alteraFlag() {
    flag = 1;
}
